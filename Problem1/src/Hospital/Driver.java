package Hospital;
//********************************************************************

//File:         Price.java       
//Author:       Liam Quinn
//Date:         November 16, 2017
//Course:       CPS100
//
//Problem Statement: 
//  Problem 1:
//  ----------
//  Design and implement a set of classes that define the employees of a hospital: 
//  doctor, nurse, administrator, surgeon, receptionist, janitor, and so on. 
//  Include methods in each class that are named according to the services 
//  provided by that person and that print an appropriate message. Create a main 
//  driver class to instantiate and exercise several of the classes.
//
//Inputs: None  
//Outputs:  Output from methods of several object from this problem
// 
//********************************************************************

public class Driver
{

  public static void main(String[] args)
  {
    Worker worker = null;
    Janitor janitor = new Janitor("Alpha", 26, 123456.78);
    
    // System.out.println(janitor);

    // worker = janitor;
    
    Receptionist receptionist = new Receptionist("Alan", 32, "Left Wing - 3rd Floor");
    // System.out.println(receptionist);
    
    Worker[] people = { janitor,  receptionist };
    
    for (int index = 0; index < people.length; index++)
    {
      System.out.println(people[index].getJobDescription());
    }
    
  }

}

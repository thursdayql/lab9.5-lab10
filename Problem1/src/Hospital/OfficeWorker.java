package Hospital;

public abstract class OfficeWorker extends Worker
{
  protected String officeLocation;
  
  public OfficeWorker(String name, int age, String officeLocation)
  {
    super(name, age);
    this.officeLocation = officeLocation;
  }
  
  public abstract String getOfficeHours();
  
  public String toString()
  {
    String str = super.toString();
    str += "Location: " + officeLocation + "\n";
    return str;
  }

}

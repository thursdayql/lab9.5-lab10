package Hospital;

public class Receptionist extends OfficeWorker
{

  public Receptionist(String name, int age, String officeLocation)
  {
    super(name, age, officeLocation);
    
  }

  public String getJobDescription()
  {
    return "Receptionist = Level 5";
  }
  
  public String getOfficeHours()
  {
    return "9AM - 5PM";
  }
   public String toString()
   {
     String str = super.toString();
     str += this.getOfficeHours() + "\n";
     
     return str;
   }
}

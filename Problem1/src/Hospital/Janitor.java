package Hospital;

public class Janitor extends Worker
{
  private double salary;
  
  public Janitor(String name, int age, double salary)
  {
    super(name, age);
    this.salary = salary;
  }
  
  public String getJobDescription()
  {
    return "Master Janitor Superior - Level 1";
  }
  
  public String toString()
  {
    String str = super.toString();
    str += "Salary: " + salary + "\n";
    return str;
  }

}

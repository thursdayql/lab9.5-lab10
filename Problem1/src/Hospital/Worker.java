package Hospital;

public abstract class Worker
{
  protected String name;
  protected int age;
  
  public Worker(String name, int age)
  {
    this.name = name;
    this.age = age;
  }
  
  public abstract String getJobDescription();
  
  public String toString()
  {
    String str = "";
    str += "Name: " + name + "\n";
    str += "Age: " + age + "\n";
    return str;
  }
}
